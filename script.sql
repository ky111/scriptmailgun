USE [EmailProxyDB ]
GO
/****** Object:  Table [dbo].[whitelists]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[whitelists](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IpAddress] [nvarchar](50) NOT NULL,
	[DescriptionAbout] [nvarchar](max) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_whitelists] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[whitelists] ADD  CONSTRAINT [DF_whitelists_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  StoredProcedure [dbo].[addIp]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[addIp]

@IpAddress nvarchar(50),
@DescriptionAbout nvarchar(MAX)
as
insert into whitelists(IpAddress,DescriptionAbout)
values(@IpAddress,@DescriptionAbout)
select IDENT_CURRENT('dbo.whitelists')

GO
/****** Object:  StoredProcedure [dbo].[checkIpAddress]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[checkIpAddress]

@IpAddress nvarchar(50)
as
select Id from whitelists 
where IpAddress= @IpAddress and IsDeleted=0

GO
/****** Object:  StoredProcedure [dbo].[deleteIp]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[deleteIp]
@Id int
as
update whitelists 
set  IsDeleted=1
where Id=@Id
select *
from whitelists
where Id=@Id
GO
/****** Object:  StoredProcedure [dbo].[getWhiteList]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getWhiteList]
as
select *
from dbo.whitelists
where IsDeleted=0
GO
/****** Object:  StoredProcedure [dbo].[updateIp]    Script Date: 30/03/2020 20:54:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[updateIp]
@Id int,
@IpAddress nvarchar(50),
@DescriptionAbout nvarchar(MAX)
as
update whitelists 
set  IpAddress=@IpAddress,DescriptionAbout=@DescriptionAbout
where Id=@Id
GO
